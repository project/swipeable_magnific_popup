/**
 * @file
 * Makes Magnific Popups swipeable.
 */

(function ($, Drupal) {
  'use strict';

  Drupal.behaviors.swipeable_magnific_popup = {
    attach: function (context) {
      $('*', context).on('mfpOpen', function(e) {
        $('.mfp-wrap').swipe({
          swipeLeft: function (event, direction, distance, duration, fingerCount) {
            $('.mfp-arrow-left').magnificPopup('next');
          },
          swipeRight: function () {
            $('.mfp-arrow-right').magnificPopup('prev');
          },
          threshold: 50
        });
      });
    }
  };

})(jQuery, Drupal);
