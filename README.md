# Swipeable Magnific Popup

## Table of contents
- Introduction
- Features
- Requirements
- Installation and configuration
- Similar modules
- Maintainer
- Credits

## Introduction
The **Swipeable Magnific Popup** module makes the
[Magnific Popup](https://www.drupal.org/project/magnific_popup) module
swipeable using the
[TouchSwipe jQuery plugin](https://github.com/mattbryson/TouchSwipe-Jquery-Plugin).

## Features
Make your Magnific Popups swipeable **without the need to add custom code!**

## Requirements
- [Magnific Popup](https://www.drupal.org/project/magnific_popup)
- [Libraries API](https://www.drupal.org/project/libraries)

## Installation and configuration
1. Install the Magnific Popup and Libraries API modules
2. Download the TouchSwipe jQuery plugin and extract it to
`sites/all/libraries/touch_swipe`
- Following file be loaded by the module:
`sites/all/libraries/touch_swipe/jquery.touchSwipe.min.js`
3. Enable the Swipeable Magnific Popup module
4. That's it, no configuration needed!

## Similar modules
- [jQuery touch swipe](https://www.drupal.org/project/jquery_touch_swipe):
Brings jQuery touch swipe events into Drupal. More flexible and not only
limited to the Magnific Popup module **but** custom coding will be required!

## Maintainer
- David Pacassi Torrico ([dpacassi](https://www.drupal.org/u/dpacassi))

## Credits
- [Liip](https://www.drupal.org/liip):
Initial development
- [Pacassi Webdesign & Development](https://www.drupal.org/pacassi-webdesign-development):
Module maintenance and support
